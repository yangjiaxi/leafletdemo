import React from 'react'
import mapboxgl from 'mapbox-gl'
import axios from 'axios'
import store from '../../Store'
import {observer} from 'mobx-react'

mapboxgl.accessToken = 'pk.eyJ1IjoieWFuZ2ppYXhpIiwiYSI6ImNqNzl0NTN0MzA2bmwzMnFtcXJ4MDNkbzMifQ.O2IKwcIyEBlcxzcLSukBKA'


let map = null

@observer
class MapBoxBaseMap extends React.Component {
  constructor() {
    super()
    axios.get('/largePoint.json')
      .then(res => {

        this.getData(res.data)
      })
  }

  getData(data) {
    const points = []

    for (let i of data) {
      try {
        if (i.latitude < 200 && i.longitude < 200) {
          points.push(i)

        }
      } catch (err) {

      }
    }

    store.change('points', points)


  }


  componentDidUpdate() {
    map = new mapboxgl.Map({
      container: 'map',
      style: 'mapbox://styles/mapbox/streets-v9'
    })
    this.addMaker()
  }

  addMaker() {


    const points = store.points

    points.map(v => {

      const el = window.document.createElement('div')
      el.style.width = 10 + 'px'
      el.style.height = 10 + 'px'
      el.style.backgroundColor = 'red'
      let locations = [v.longitude, v.latitude]
      let newMaker = new mapboxgl.Marker(el)
        .setLngLat(locations)
        .addTo(map)

      return newMaker
    })


  }

  render() {
    // eslint-disable-next-line

    const points = store.points
    return (
      <div id='map' style={{height: '100%', width: '100%',}}></div>
    )
  }
}

export default MapBoxBaseMap