import React from 'react'
import L from 'leaflet'
import 'leaflet.markercluster'
import './style/MarkerCluster.css'
import './style/MarkerCluster.Default.css'
import axios from 'axios'
import {observer} from 'mobx-react'
import store from '../Store'

const accessToken = 'pk.eyJ1IjoieWFuZ2ppYXhpIiwiYSI6ImNqNzl0NTN0MzA2bmwzMnFtcXJ4MDNkbzMifQ.O2IKwcIyEBlcxzcLSukBKA'
let mymap = null


@observer
class BaseMap extends React.Component {

  constructor() {
    super()
    axios.get('/largePoint.json')
      .then(res => {
        this.getData(res.data)
      })
  }

  getData(data) {
    const points = []

    for (let i of data) {
      try {
        if (i.latitude < 200 && i.longitude < 200) {
          points.push(i)

        }
      } catch (err) {

      }
    }
    store.change('points', points)


  }

  componentDidUpdate() {
    mymap = L.map('mapid', {
      center: [30.54, 104.06],
      zoom: 13,
      preferCanvas: true,
    })

    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
      id: 'mapbox.streets',
      accessToken,
    }).addTo(mymap)
    this.addMarker()

  }

  addMarker() {
    const b = []
    const markers = L.markerClusterGroup()
    // eslint-disable-next-line
    store.points.map(v => {
      const locations = [v.latitude, v.longitude]
      const newMarker = L.marker(locations)
      b.push(newMarker)
      markers.addLayer(newMarker)
      newMarker.bindPopup(`${v.id}`)
    })
    const group = new L.featureGroup(b)
    mymap.addLayer(markers)
    mymap.fitBounds(group.getBounds())

  }


  render() {
    // eslint-disable-next-line
    const points = store.points
    return (
      <div id="mapid"></div>
    )
  }
}


export default BaseMap


//TODO:点击一个popup会关闭上一个popup