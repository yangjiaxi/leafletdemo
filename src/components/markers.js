import React from 'react'
import L from 'leaflet'
import axios from 'axios'
import {observer} from 'mobx-react'
import store from '../Store'

const accessToken = 'pk.eyJ1IjoieWFuZ2ppYXhpIiwiYSI6ImNqNzl0NTN0MzA2bmwzMnFtcXJ4MDNkbzMifQ.O2IKwcIyEBlcxzcLSukBKA'
let mymap = null

@observer
class BaseMap extends React.Component {

  constructor() {
    super()
    axios.get('/point.json')
      .then(res => {
        this.getData(res.data.data)
      })
  }

  getData(data) {
    const points = []

    for (let i of data) {
      try {
        if (i.last_valid_gps.latitude < 200 && i.last_valid_gps.longitude < 200) {
          points.push(i)

        }
      } catch (err) {

      }
    }
    store.change('points', points)


  }

  componentDidUpdate() {
    mymap = L.map('mapid', {
      center: [30.54, 104.06],
      zoom: 13,
      preferCanvas: true,
    })

    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
      id: 'mapbox.streets',
      accessToken,
    }).addTo(mymap)
    this.addMarker()

  }

  addMarker() {
    const b = []
    // eslint-disable-next-line
    let points = []
    for (let i = 0; i < 25; i++) {
      points = [...points, ...store.points]
    }
    console.log(points)
    points.map(v => {

      const locations = [v.last_valid_gps.latitude, v.last_valid_gps.longitude]
      const newMarker = L.marker(locations).addTo(mymap)
      b.push(newMarker)
      newMarker.bindPopup(`${v.id}`)
    })
    const group = new L.featureGroup(b)
    mymap.fitBounds(group.getBounds())

  }


  render() {
    // eslint-disable-next-line
    const points = store.points
    return (
      <div id="mapid"></div>
    )
  }
}


export default BaseMap


//TODO:点击一个popup会关闭上一个popup