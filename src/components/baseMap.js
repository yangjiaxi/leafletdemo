import React from 'react'
import L from 'leaflet'


const accessToken = 'pk.eyJ1IjoieWFuZ2ppYXhpIiwiYSI6ImNqNzl0NTN0MzA2bmwzMnFtcXJ4MDNkbzMifQ.O2IKwcIyEBlcxzcLSukBKA'
let mymap = null


class BaseMap extends React.Component {

  componentDidMount() {


    mymap = L.map('mapid').setView([51.505, -0.09], 13)
    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
      id: 'mapbox.streets',
      accessToken,
    }).addTo(mymap)
    this.addMarker()
    this.addCircle()
  }

  addMarker() {
    const marker = L.marker([51.5, -0.09]).addTo(mymap)
    marker.bindPopup(`
    <p style="color: #ce4250">I am a marker</p>
    <button style="padding: 10px;" id='btn'}>See More</button>

    `).openPopup()
    const btn = window.document.querySelector('#btn')

    btn.onclick = () => {
      console.log('hello')
    }
  }

  addCircle() {
    const circle = L.circle([51.508, -0.11], {
      color: 'red',
      fillColor: '#ffb0b2',
      fillOpacity: 0.5,
      radius: 500
    }).addTo(mymap)
    circle.bindPopup("I am a circle.").openPopup()
  }

  render() {
    return (
      <div id="mapid"></div>
    )
  }
}


export default BaseMap