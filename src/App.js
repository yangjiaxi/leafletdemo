import React, {Component} from 'react'
// eslint-disable-next-line
import BaseMap from './components/baseMap'
// eslint-disable-next-line
import MarkersMap from './components/markers'
// eslint-disable-next-line
import MarkerCluster from './components/leaflet.markercluster'

import MapBoxBaseMap from './components/mapbox/baseMap'

class App extends Component {


  render() {
    return (
      <div className="App">
        <MarkerCluster/>
      </div>
    )
  }
}

export default App
