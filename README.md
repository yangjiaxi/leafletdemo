
1. [React-Map-Gl](https://uber.github.io/react-map-gl/#/) [DECK.GL](https://uber.github.io/deck.gl/#/) 与`React`集成较高,

适配geojson数据,可直接调用两者给的组件,结合数据可以快速生成展示类的地图.使用较为方便.

优势:大数据量下,渲染速度快

适用于:柱状图,线路图等.

2. 在react中使用原生 [MapBox](https://www.mapbox.com/mapbox-gl-js/api/)时,

需要与`React`生命周期函数配合,在合适的时间在指定的DOM上生成地图,并且当数据更新时同步更新地图.

优势:API众多,可定制化高.

3. [leaflet](http://leafletjs.com/)可以简化操作地图的流程,插件丰富,在数据量小时,可以选择使用.

缺陷:部分浏览器部分情况下会出现bug(左右拖拽地图时,瓦片间会显现白线)

适用于:围栏的绘制与修改



- [柱状图](https://uber.github.io/deck.gl/#/examples/core-layers/hexagon-layer)
- [线路图](https://uber.github.io/deck.gl/#/examples/core-layers/line-layer)
- [Marker PopUp](https://uber.github.io/react-map-gl/#/Examples/markers-popups)
- [热力图](https://www.mapbox.com/mapbox-gl-js/example/heatmap-layer/)
- [MapBox集群](https://www.mapbox.com/mapbox-gl-js/example/cluster/)
- [可用做迁徙的地图](https://www.mapbox.com/mapbox-gl-js/example/rotating-controllable-marker/)
- leaflet集群在src/components/leaflet.markercluster.js











